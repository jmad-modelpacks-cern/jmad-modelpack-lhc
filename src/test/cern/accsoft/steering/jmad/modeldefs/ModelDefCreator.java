package cern.accsoft.steering.jmad.modeldefs;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import cern.accsoft.steering.jmad.modeldefs.defs.LhcB4ModelDefinitionFactory2017;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2014;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2015;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2015thin;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2016;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2017;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2018;
import cern.accsoft.steering.jmad.modeldefs.defs.LhcNominalModelDefinitionFactory2020;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.io.ModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.modeldefs.io.impl.ModelDefinitionUtil;
import cern.accsoft.steering.jmad.modeldefs.io.impl.XmlModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.util.xml.PersistenceServiceException;

public class ModelDefCreator {

    public final static void main(String[] args) {

        String destPath = "";
        if (args.length > 0) {
            destPath = args[0];
        }

        BasicConfigurator.configure();

        ModelDefinitionFactory[] factories = new ModelDefinitionFactory[] { //
               new LhcNominalModelDefinitionFactory2014(),
               new LhcNominalModelDefinitionFactory2015(),
               new LhcNominalModelDefinitionFactory2016(),
               new LhcNominalModelDefinitionFactory2017(),
               new LhcNominalModelDefinitionFactory2018(),
               new LhcNominalModelDefinitionFactory2020(),
               new LhcB4ModelDefinitionFactory2017(),
               new LhcNominalModelDefinitionFactory2015thin()
        };

        ModelDefinitionPersistenceService service = new XmlModelDefinitionPersistenceService();

        for (ModelDefinitionFactory factory : factories) {
            JMadModelDefinition modelDefinition = factory.create();
            String fileName = ModelDefinitionUtil.getProposedXmlFileName(modelDefinition);
            String filePath;
            if (destPath.length() > 0) {
                filePath = destPath + "/" + fileName;
            } else {
                filePath = "src/java/cern/accsoft/steering/jmad/modeldefs/defs/" + fileName;
            }
            File file = new File(filePath);
            System.out.println("Writing file '" + file.getAbsolutePath() + "'.");
            try {
                service.save(modelDefinition, file);
            } catch (PersistenceServiceException e) {
                System.out.println("Could not save model definition to file '" + file.getAbsolutePath());
                e.printStackTrace();
            }
        }

    }
}
