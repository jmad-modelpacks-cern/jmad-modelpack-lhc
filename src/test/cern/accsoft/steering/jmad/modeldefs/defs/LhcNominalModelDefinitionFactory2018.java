/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.REPOSITORY;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2018 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2018.seq", REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2018");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2018";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2018 optics */
        definitionSetList.add(this.create2018AtsLowBetaRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018AtsIonRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018AtsHighTeleRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018HighBetaOptics());

        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     * 
     * @return
     */
    private OpticDefinitionSet create2018AtsLowBetaRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        // builder.addFinalCommonOpticFile(
        // OpticModelFileBuilder.createInstance("match-chroma.madx").isResource().doNotParse());
        /* ramp and squeeze to 3m in 1,5 and 8 */
        builder.addOptic("R2017a_A11mC11mA10mL10m", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_11m_fixQ8L4.madx") });

        builder.addOptic("R2017a_A970C970A10mL970", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_970cm.madx") });

        builder.addOptic("R2017a_A920C920A10mL920", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_920cm.madx") });

        builder.addOptic("R2017a_A850C850A10mL850", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_850cm.madx") });

        builder.addOptic("R2017a_A740C740A10mL740", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_740cm.madx") });

        builder.addOptic("R2017a_A630C630A10mL630", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_630cm.madx") });

        builder.addOptic("R2017a_A530C530A10mL530", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_530cm.madx") });

        builder.addOptic("R2017a_A440C440A10mL440", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_440cm.madx") });

        builder.addOptic("R2017a_A360C360A10mL360", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_360cm.madx") });

        builder.addOptic("R2017a_A310C310A10mL310", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_310cm.madx") });

        builder.addOptic("R2017a_A230C230A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_230cm.madx") });

        builder.addOptic("R2017a_A180C180A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_180cm.madx") });

        builder.addOptic("R2017a_A135C135A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_135cm.madx") });

        builder.addOptic("R2017a_A100C100A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_100cm.madx") });

        builder.addOptic("R2017a_A80C80A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_80cm.madx") });

        builder.addOptic("R2017a_A65C65A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_65cm.madx") });

        builder.addOptic("R2017a_A54C54A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_54cm.madx") });

        builder.addOptic("R2017a_A46C46A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_46cm.madx") });

        builder.addOptic("R2017a_A40C40A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_40cm.madx") });

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_40cm_ctpps1.madx") });

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_40cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_37cm_ctpps1.madx") });

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_37cm_ctpps2.madx") });
 
        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS1", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_33cm_ctpps1.madx") });

        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_33cm_ctpps2.madx") });

        builder.addOptic("R2017aT_A30C30A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_30cm_ctpps2.madx") });

        builder.addOptic("R2018aT_A27C27A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_27cm_ctpps2.madx") });

        builder.addOptic("R2018aT_A25C25A10mL300_CTPPS2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_25cm_ctpps2.madx") });

        return builder.build();
    }

    /**
     * ATS ramp and squeeze for 2018 ions
     * 
     * @return
     */
    private OpticDefinitionSet create2018AtsIonRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
 
        builder.addOptic("R2018i_A970C970A970L970", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_970cm.madx") });

        builder.addOptic("R2018i_A920C920A920L920", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_920cm.madx") });

        builder.addOptic("R2018i_A850C850A850L850", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_850cm.madx") });

        builder.addOptic("R2018i_A760C760A760L760", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_760cm.madx") });

        builder.addOptic("R2018i_A670C670A670L670", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_670cm.madx") });

        builder.addOptic("R2018i_A590C590A590L590", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_590cm.madx") });

        builder.addOptic("R2018i_A520C520A520L520", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_520cm.madx") });

        builder.addOptic("R2018i_A450C450A450L450", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_450cm.madx") });

        builder.addOptic("R2018i_A400C400A400L400", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_400cm.madx") });

        builder.addOptic("R2018i_A360C360A360L360", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_360cm.madx") });

        builder.addOptic("R2018i_A320C320A320L320", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_320cm.madx") });

        builder.addOptic("R2018i_A290C290A290L290", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_290cm.madx") });

        builder.addOptic("R2018i_A230C230A230L230", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_230cm.madx") });

        builder.addOptic("R2018i_A185C185A185L185", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_185cm.madx") });

        builder.addOptic("R2018i_A135C135A135L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_135cm.madx") });

        builder.addOptic("R2018i_A100C100A100L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_100cm.madx") });

        builder.addOptic("R2018i_A82C82A82L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_82cm.madx") });

        builder.addOptic("R2018i_A68C68A68L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_68cm.madx") });

        builder.addOptic("R2018i_A57C57A57L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_57cm.madx") });

        builder.addOptic("R2018i_A50C50A50L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_50cm.madx") });

        builder.addOptic("R2018i_A44C44A50L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_44cm.madx") });

        builder.addOptic("R2018i_A40C40A50L150", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ats/ions/ats-ion_40cm.madx") });

        return builder.build();
    }
    
    
    /**
     * ATS high telescope index ramp and squeeze for 2018 MDs
     * 
     * @return
     */
    private OpticDefinitionSet create2018AtsHighTeleRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
 
//        builder.addOptic("R2018aHT_A11mC11mA10mL10m", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/11m.madx") });
//
//        builder.addOptic("R2018aHT_A970C970A10mL970", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/970cm.madx") });
//
//        builder.addOptic("R2018aHT_A920C920A10mL920", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/920cm.madx") });
//
//        builder.addOptic("R2018aHT_A850C850A10mL850", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/850cm.madx") });
//
//        builder.addOptic("R2018aHT_A740C740A10mL740", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/740cm.madx") });
//
//        builder.addOptic("R2018aHT_A630C630A10mL630", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/630cm.madx") });
//
//        builder.addOptic("R2018aHT_A530C530A10mL530", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/530cm.madx") });
//
//        builder.addOptic("R2018aHT_A440C440A10mL440", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/440cm.madx") });
//
//        builder.addOptic("R2018aHT_A360C360A10mL360", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/360cm.madx") });
//
//        builder.addOptic("R2018aHT_A310C310A10mL310", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/310cm.madx") });
//
//        builder.addOptic("R2018aHT_A230C230A10mL300", new OpticModelFileBuilder[] {
//                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/230cm.madx") });

        builder.addOptic("R2018a_A200C200A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/200cm.madx") });
        
        builder.addOptic("R2018aT200_A182C182A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/182cm.madx") });

        builder.addOptic("R2018aT200_A155C155A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/155cm.madx") });

        builder.addOptic("R2018aT200_A122C122A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/122cm.madx") });

        builder.addOptic("R2018aT200_A95C95A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/95cm.madx") });

        builder.addOptic("R2018aT200_A77C77A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/77cm.madx") });

        builder.addOptic("R2018aT200_A65C65A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/65cm.madx") });

        builder.addOptic("R2018aT172_A56C56A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/56cm.madx") });

        builder.addOptic("R2018aT145_A47C47A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/47cm.madx") });

        builder.addOptic("R2018aT123_A40C40A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/40cm.madx") });

        builder.addOptic("R2018aT105_A34C34A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/34cm.madx") });

        builder.addOptic("R2018aT92_A30C30A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/30cm.madx") });

        builder.addOptic("R2018aT83_A27C27A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/27cm.madx") });

        builder.addOptic("R2018aT77_A25C25A10mL300", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_RoundHighTele/25cm.madx") });
        
        return builder.build();
    }
    
    
    
    
    private OpticDefinitionSet create2018HighBetaOptics() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();
        
        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(OpticModelFileBuilder.createInstance("strength/ats/low-beta/ats_11m_fixQ8L4.madx"));
        
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll-rq-ats.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-tune-coll-ats.madx").isResource()
                .doNotParse());
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("match-chroma-ats.madx").isResource()
                .doNotParse());

                       
        /* squeeze sequence down to 90 m in all 1+5 */

        builder.addOptic(
                "R2018h_A12mC12mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0012.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0012.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0012_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0012_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.12m.madx")});

        builder.addOptic(
                "R2018h_A14mC14mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0014.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0014.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0014_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0014_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A16mC16mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0016.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0016.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0016_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0016_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A19mC19mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0019.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0019.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0019_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0019_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A22mC22mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0022.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0022.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0022_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0022_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A25mC25mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0025.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0025.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0025_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0025_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A30mC30mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0030.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0030.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0030_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0030_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A33mC33mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0033.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0033.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0033_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0033_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A36mC36mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0036.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0036.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0036_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0036_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A40mC40mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0040.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0040.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0040_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0040_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A43mC43mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0043.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0043.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0043_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0043_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A46mC46mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0046.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0046.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0046_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0046_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A51mC51mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0051.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0051.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0051_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0051_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A54mC54mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0054.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0054.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0054_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0054_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A60mC60mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0060.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0060.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0060_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0060_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A67mC67mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0067.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0067.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0067_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0067_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A75mC75mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0075.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0075.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0075_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0075_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A82mC82mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0082.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0082.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0082_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0082_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });

        builder.addOptic(
                "R2018h_A90mC90mA10mL10m",
                new OpticModelFileBuilder[] {OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0090.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0090.madx"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR1/IP1_0090_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/IR5/IP5_0090_bump.str"),
                        OpticModelFileBuilder.createInstance("strength/highbeta90m/mq-cor.madx") });
        

        return builder.build();
    }
    

}