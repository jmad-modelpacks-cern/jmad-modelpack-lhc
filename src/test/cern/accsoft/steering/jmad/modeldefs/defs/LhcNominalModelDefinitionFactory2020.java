/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.REPOSITORY;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

/**
 * The model definition factory for the LHC model for 2015 23 Jan 2015: Second version, introduce version 'c' for the
 * medium beta based on other beta* point (from HB's 90 m).
 * 
 * @author muellerg
 */
public class LhcNominalModelDefinitionFactory2020 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("init-constants.madx", RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/lhc2018_modifiedIR7_V2.seq", REPOSITORY));
    }

    @Override
    protected ModelPathOffsets createModelPathOffsets() {
        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryOffset("2020");
        return offsets;
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2020";
    }

    /**
     * @param rangeDefinition
     */
    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        // TODO Auto-generated method stub

    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<OpticDefinitionSet>();

        /* 2020 optics */
        definitionSetList.add(this.create2020AtsRampSqueezeOpticsSet());

        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     * 
     * @return
     */
    private OpticDefinitionSet create2020AtsRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(OpticModelFileBuilder.createInstance("reset-bump-flags.madx").isResource()
                .parseAs(ParseType.STRENGTHS));
        // builder.addFinalCommonOpticFile(
        // OpticModelFileBuilder.createInstance("match-chroma.madx").isResource().doNotParse());
        /* ramp and squeeze to 1m in 1,5 */
        builder.addOptic("R2022a_A11mC11mA10mL10m_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/11m.madx") });

        builder.addOptic("R2022a_A10mC10mA10mL10m_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/10m.madx") });

        builder.addOptic("R2022a_A960cmC960cmA10mL960cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/960cm.madx") });

        builder.addOptic("R2022a_A910cmC910cmA10mL910cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/910cm.madx") });

        builder.addOptic("R2022a_A830cmC830cmA10mL830cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/830cm.madx") });

        builder.addOptic("R2022a_A710cmC710cmA10mL710cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/710cm.madx") });

        builder.addOptic("R2022a_A590cmC590cmA10mL590cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/590cm.madx") });

        builder.addOptic("R2022a_A490cmC490cmA10mL490cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/490cm.madx") });

        builder.addOptic("R2022a_A410cmC410cmA10mL410cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/410cm.madx") });

        builder.addOptic("R2022a_A330cmC330cmA10mL330cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/330cm.madx") });

        builder.addOptic("R2022a_A260cmC260cmA10mL260cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/260cm.madx") });

        builder.addOptic("R2022a_A210cmC210cmA10mL210cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/210cm.madx") });

        builder.addOptic("R2022a_A170cmC170cmA10mL170cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/170cm.madx") });

        // Test of new telescopic optics

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1-12_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_1-12.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1-33_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_1-33.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1-53_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_1-53.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1-86_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_1-86.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-2-24_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_2-24.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-2-50_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V1/150cm_2-50.madx") });

        // Test is finished

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1.15_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1-15.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1.3_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1-3.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1.45_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1-45.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1.66_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1-66.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-1.97_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_1-97.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-2.27_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_2-27.madx") });

        builder.addOptic("R2022a_A150cmC150cmA10mL150cm_ind-2.5_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/150cm_2-5.madx") });

        builder.addOptic("R2022a_A133cmC133cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/133cm.madx") });

        builder.addOptic("R2022a_A116cmC116cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/116cm.madx") });

        builder.addOptic("R2022a_A100cmC100cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/100cm.madx") });

        builder.addOptic("R2022a_A84cmC84cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/84cm.madx") });

        builder.addOptic("R2022a_A71cmC71cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/71cm.madx") });

        builder.addOptic("R2022a_A60cmC60cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/60cm.madx") });

        builder.addOptic("R2022a_A51cmC51cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/51cm.madx") });

        builder.addOptic("R2022a_A43cmC43cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/43cm.madx") });

        builder.addOptic("R2022a_A36cmC36cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/36cm.madx") });

        builder.addOptic("R2022a_A30cmC30cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/30cm.madx") });

        builder.addOptic("R2022a_A25cmC25cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/25cm.madx") });

        builder.addOptic("R2022a_A22cmC22cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/22cm.madx") });

        builder.addOptic("R2022a_A20cmC20cmA10mL150cm_test", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022/20cm.madx") });

        // New test october 2020

        builder.addOptic("R2022a_A11mC11mA10mL10m_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/11m.madx") });

        builder.addOptic("R2022a_A10mC10mA10mL10m_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/10m.madx") });

        builder.addOptic("R2022a_A960cmC960cmA10mL960cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/960cm.madx") });

        builder.addOptic("R2022a_A910cmC910cmA10mL910cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/910cm.madx") });

        builder.addOptic("R2022a_A840cmC840cmA10mL840cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/840cm.madx") });

        builder.addOptic("R2022a_A730cmC730cmA10mL730cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/730cm.madx") });

        builder.addOptic("R2022a_A610cmC610cmA10mL610cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/610cm.madx") });

        builder.addOptic("R2022a_A500cmC500cmA10mL500cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/500cm.madx") });

        builder.addOptic("R2022a_A410cmC410cmA10mL410cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/410cm.madx") });

        builder.addOptic("R2022a_A330cmC330cmA10mL330cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/330cm.madx") });

        builder.addOptic("R2022a_A270cmC270cmA10mL270cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/270cm.madx") });

        builder.addOptic("R2022a_A220cmC220cmA10mL220cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/220cm.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/200cm.madx") });

        builder.addOptic("R2022a_A183cmC183cmA10mL183cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/183cm.madx") });

        builder.addOptic("R2022a_A172cmC172cmA10mL172cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/172cm.madx") });

        builder.addOptic("R2022a_A159cmC159cmA10mL159cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/159cm.madx") });

        builder.addOptic("R2022a_A143cmC143cmA10mL143cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/143cm.madx") });

        builder.addOptic("R2022a_A120cmC120cmA10mL120cm_test2", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V2/120cm.madx") });
        
        // New test November 2020

        builder.addOptic("R2022a_A11mC11mA10mL10m_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/11m.madx") });

        builder.addOptic("R2022a_A10mC10mA10mL10m_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/10m.madx") });

        builder.addOptic("R2022a_A960cmC960cmA10mL960cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/960cm.madx") });

        builder.addOptic("R2022a_A910cmC910cmA10mL910cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/910cm.madx") });

        builder.addOptic("R2022a_A840cmC840cmA10mL840cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/840cm.madx") });

        builder.addOptic("R2022a_A750cmC750cmA10mL750cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/750cm.madx") });

        builder.addOptic("R2022a_A650cmC650cmA10mL650cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/650cm.madx") });

        builder.addOptic("R2022a_A550cmC550cmA10mL550cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/550cm.madx") });

        builder.addOptic("R2022a_A470cmC470cmA10mL470cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/470cm.madx") });

        builder.addOptic("R2022a_A400cmC400cmA10mL400cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/400cm.madx") });

        builder.addOptic("R2022a_A330cmC330cmA10mL330cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/330cm.madx") });

        builder.addOptic("R2022a_A270cmC270cmA10mL270cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/270cm.madx") });

        builder.addOptic("R2022a_A220cmC220cmA10mL220cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/220cm.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_083_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm_083.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_073_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm_073.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_065_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm_065.madx") });

        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_057_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm_057.madx") });
        
        builder.addOptic("R2022a_A200cmC200cmA10mL200cm_050_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/200cm_050.madx") });

        builder.addOptic("R2022a_A156cmC156cmA10mL200cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/156cm.madx") });

        builder.addOptic("R2022a_A120cmC120cmA10mL200cm_test3", new OpticModelFileBuilder[] {
                OpticModelFileBuilder.createInstance("strength/ATS_Nominal/Test2022_V3/120cm.madx") });

        return builder.build();
    }

}